import { GraphQLResult } from "@aws-amplify/api-graphql";
import Amplify, { API, graphqlOperation } from "aws-amplify";
import React, { FormEvent, useEffect, useRef, useState } from "react";
import {
  ListTreasureContributionsQuery,
  TreasureContribution,
} from "./app/app-sync.service";
import { createTreasureContribution } from "./graphql/mutations";
import { listTreasureContributions } from "./graphql/queries";
import { withAuthenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";

const App: React.FunctionComponent = ({ signOut, user }: any) => {
  const contributionInput = useRef<HTMLInputElement>(null);
  const [treasure, setTreasure] = useState<number | null>(null);

  useEffect(() => {
    console.log(user);
    Amplify.configure({
      aws_appsync_authenticationType: "AMAZON_COGNITO_USER_POOLS",
    });
    fetchTreasureContributions();
  }, [user]);

  async function fetchTreasureContributions() {
    try {
      const treasureData = (await API.graphql(
        graphqlOperation(listTreasureContributions)
      )) as GraphQLResult<ListTreasureContributionsQuery>;

      if (treasureData.data?.listTreasureContributions) {
        // transform all treasure contributions into a number
        const treasure =
          treasureData.data.listTreasureContributions.items.reduce(
            (p: number, c: TreasureContribution) =>
              (p += parseInt(c.value, 10)),
            0
          );
        console.log(`Current treasure: ${treasure}`);
        setTreasure(treasure);
      } else {
        console.error("Couldn't found {listTreasureContributions}");
      }
    } catch (err) {
      console.error("Error fetching contributions");
      console.error(err);
    }
  }

  async function handleSubmitContribution(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    try {
      if (contributionInput.current) {
        await API.graphql(
          graphqlOperation(createTreasureContribution, {
            input: {
              value: contributionInput.current.value,
              currency: "AED",
            },
          })
        );

        contributionInput.current.value = "";
        fetchTreasureContributions();
      }
    } catch (err) {
      console.error("Error adding contribution");
      console.error(err);
    }
  }

  return (
    <div style={{ paddingLeft: 16 }}>
      <h1>Hello {user.attributes.email}</h1>
      <h1>
        {treasure !== null
          ? `Current treasure: ${treasure}`
          : "Loading treasure..."}
      </h1>

      <form onSubmit={handleSubmitContribution}>
        <input ref={contributionInput} type="number" />
        <button type="submit">Submit Contribution</button>
      </form>

      <button
        onClick={signOut}
        style={{
          marginTop: 24,
          padding: "12px 24px",
          backgroundColor: "#6667ab",
          color: "#fff",
          cursor: "pointer",
        }}
      >
        Sign out
      </button>
    </div>
  );
};

export default withAuthenticator(App);
