/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateTreasureContributionInput = {
  id?: string | null,
  value: string,
  currency: string,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type ModelTreasureContributionConditionInput = {
  value?: ModelStringInput | null,
  currency?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelTreasureContributionConditionInput | null > | null,
  or?: Array< ModelTreasureContributionConditionInput | null > | null,
  not?: ModelTreasureContributionConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type TreasureContribution = {
  __typename: "TreasureContribution",
  id: string,
  value: string,
  currency: string,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type UpdateTreasureContributionInput = {
  id: string,
  value?: string | null,
  currency?: string | null,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type DeleteTreasureContributionInput = {
  id: string,
};

export type ModelTreasureContributionFilterInput = {
  id?: ModelIDInput | null,
  value?: ModelStringInput | null,
  currency?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelTreasureContributionFilterInput | null > | null,
  or?: Array< ModelTreasureContributionFilterInput | null > | null,
  not?: ModelTreasureContributionFilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelTreasureContributionConnection = {
  __typename: "ModelTreasureContributionConnection",
  items:  Array<TreasureContribution >,
  nextToken?: string | null,
};

export type CreateTreasureContributionMutationVariables = {
  input: CreateTreasureContributionInput,
  condition?: ModelTreasureContributionConditionInput | null,
};

export type CreateTreasureContributionMutation = {
  createTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type UpdateTreasureContributionMutationVariables = {
  input: UpdateTreasureContributionInput,
  condition?: ModelTreasureContributionConditionInput | null,
};

export type UpdateTreasureContributionMutation = {
  updateTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type DeleteTreasureContributionMutationVariables = {
  input: DeleteTreasureContributionInput,
  condition?: ModelTreasureContributionConditionInput | null,
};

export type DeleteTreasureContributionMutation = {
  deleteTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type GetTreasureContributionQueryVariables = {
  id: string,
};

export type GetTreasureContributionQuery = {
  getTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type ListTreasureContributionsQueryVariables = {
  filter?: ModelTreasureContributionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTreasureContributionsQuery = {
  listTreasureContributions?:  {
    __typename: "ModelTreasureContributionConnection",
    items:  Array< {
      __typename: "TreasureContribution",
      id: string,
      value: string,
      currency: string,
      createdAt?: string | null,
      updatedAt?: string | null,
    } >,
    nextToken?: string | null,
  } | null,
};

export type OnCreateTreasureContributionSubscription = {
  onCreateTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type OnUpdateTreasureContributionSubscription = {
  onUpdateTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};

export type OnDeleteTreasureContributionSubscription = {
  onDeleteTreasureContribution?:  {
    __typename: "TreasureContribution",
    id: string,
    value: string,
    currency: string,
    createdAt?: string | null,
    updatedAt?: string | null,
  } | null,
};
