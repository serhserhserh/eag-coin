/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getTreasureContribution = /* GraphQL */ `
  query GetTreasureContribution($id: ID!) {
    getTreasureContribution(id: $id) {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
export const listTreasureContributions = /* GraphQL */ `
  query ListTreasureContributions(
    $filter: ModelTreasureContributionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTreasureContributions(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        value
        currency
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
