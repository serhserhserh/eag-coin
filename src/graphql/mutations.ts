/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createTreasureContribution = /* GraphQL */ `
  mutation CreateTreasureContribution(
    $input: CreateTreasureContributionInput!
    $condition: ModelTreasureContributionConditionInput
  ) {
    createTreasureContribution(input: $input, condition: $condition) {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
export const updateTreasureContribution = /* GraphQL */ `
  mutation UpdateTreasureContribution(
    $input: UpdateTreasureContributionInput!
    $condition: ModelTreasureContributionConditionInput
  ) {
    updateTreasureContribution(input: $input, condition: $condition) {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
export const deleteTreasureContribution = /* GraphQL */ `
  mutation DeleteTreasureContribution(
    $input: DeleteTreasureContributionInput!
    $condition: ModelTreasureContributionConditionInput
  ) {
    deleteTreasureContribution(input: $input, condition: $condition) {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
