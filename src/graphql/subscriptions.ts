/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTreasureContribution = /* GraphQL */ `
  subscription OnCreateTreasureContribution {
    onCreateTreasureContribution {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTreasureContribution = /* GraphQL */ `
  subscription OnUpdateTreasureContribution {
    onUpdateTreasureContribution {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTreasureContribution = /* GraphQL */ `
  subscription OnDeleteTreasureContribution {
    onDeleteTreasureContribution {
      id
      value
      currency
      createdAt
      updatedAt
    }
  }
`;
